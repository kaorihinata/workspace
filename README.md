# Workspace

Workspace is a collection of tools, and a directory hierarchy for DevOps
tasks that makes it simple to mix and match different DevOps tools (at the
moment, I use Terraform and Ansible together.)

## `Tools/prepared`

`prepared` allows you to execute commands within a prepared environment.
Upon execution `prepared` will search for a `.prepared.subr` file and source
it. If you've define `prepared_before` and/or `prepared_after`, then they
will be executed before and after a tool is called. This allows you to do
things like:

* Create, modify or destroy environment variables.
* Create, modify or destroy aliases.
* Create, modify or destroy configuration files.

...and so on.

It's design also allows you to wrap tools with it, so if you wanted to, for
example, wrap `terraform` with it, you could create an alias like:

```sh
alias terraform="prepared --levels 2 /path/to/terraform"
```

This would create an alias for `terraform` that searches 2 levels up for
`.prepared.subr`, sources it if available, then executes `/path/to/terraform`
as described above.

## `Tools/strand`

Very simply, a tool for generating random strings. It's intended mainly for
generating passwords, and requires only a POSIX shell to do it. By default,
creates an alphanumeric password with a length of 32 characters. For more
information, a help flag is available (`-h` or `--help`.)

## `Tools/svenv`

Short for Simple Virtual Environment (see: Python.) Creates a virtual
environment based on the current platform, and first found Python
interpreter with the intention of making the creation, management and
destruction of a virtual environment as simple as possible.

If you wanted to, for example, install a module in the current directory,
and then run a script from it, you could do something as simple as:

```sh
svenv pip install -e .
svenv ./scripts/some_script.py
```
