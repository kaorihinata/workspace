# Shared

This directory is primarily for resources that are shared between multiple
workspaces. For example: Ansible playbooks, Terraform modules, and so on.
