#! /bin/sh

# secret_create {{{
secret_create() {
    _REPLACE=0
    while [ -n "$1" ]; do
        case "$1" in
            -h|--help)
                cat >&2 <<'EOF'
usage: secret create [-h/--help] [--replace] NAME FILE

optional arguments:
  -h/--help  display this message and exit
  --replace  replace an existing secret

positional arguments:
  NAME  the name of the secret
  FILE  the path to the secret file (or - for stdin)
EOF
                return 0 ;;
            --replace) _REPLACE=1; shift;;
            # {{{
            -?|--?*)
                printf -- '%s: error: unknown optional argument "%s"\n' \
                    "${0##*/}" "$1" >&2
                return 2 ;;
            -?*)
                _TAIL="${1#??}" _HEAD="${1%"$_TAIL"}"
                shift
                set -- "$_HEAD" "-$_TAIL" "$@" ;;
            *) break;;
            # }}}
        esac
    done
    if [ -z "$1" ]; then
        printf '%s: error: missing argument NAME\n' "${0##*/}" >&2
        return 2
    elif ! expr "$1" : '^[0-9A-Z_a-z-]\{1,\}$' >/dev/null; then
        printf '%s: error: invalid name format "%s"\n' "${0##*/}" "$1" >&2
        return 2
    elif secret_exists "$1" && [ "$_REPLACE" -ne "1" ]; then
        printf '%s: error: secret exists "%s"\n' "${0##*/}" "$1" >&2
        return 2
    elif [ -z "$2" ]; then
        printf '%s: error: missing argument FILE\n' "${0##*/}" >&2
        return 2
    fi
    _NAME="$1"
    _FILE="$2"
    set -- -aeo "$SECRETS_DIR/$_NAME.gpg" -qr "$SECRETS_GPGUSERID" \
        -su "$SECRETS_GPGUSERID"
    if [ "$_REPLACE" -eq "1" ]; then
        set -- "$@" --yes
    fi
    if [ "$_FILE" = "-" ] || [ -f "$_FILE" ]; then
        gpg "$@" "$_FILE"
    else
        printf '%s: error: no such file "%s"\n' "${0##*/}" "$_FILE" >&2
        return 2
    fi
}
# }}}

# secret_exists {{{
secret_exists() {
    while [ -n "$1" ]; do
        case "$1" in
            -h|--help)
                cat >&2 <<'EOF'
usage: secret exists [-h/--help] NAME

optional arguments:
  -h/--help  display this message and exit

positional arguments:
  NAME  the name of the secret
EOF
                return 0 ;;
            # {{{
            -?|--?*)
                printf -- '%s: error: unknown optional argument "%s"\n' \
                    "${0##*/}" "$1" >&2
                return 2 ;;
            -?*)
                _TAIL="${1#??}" _HEAD="${1%"$_TAIL"}"
                shift
                set -- "$_HEAD" "-$_TAIL" "$@" ;;
            *) break;;
            # }}}
        esac
    done
    if [ -z "$1" ]; then
        printf '%s: error: missing argument NAME\n' "${0##*/}" >&2
        return 2
    elif ! expr "$1" : '^[0-9A-Z_a-z-]\{1,\}$' >/dev/null; then
        printf '%s: error: invalid name format "%s"\n' "${0##*/}" "$1" >&2
        return 2
    elif [ -f "$SECRETS_DIR/$1.gpg" ]; then
        return 0
    fi
    return 1
}
# }}}

# secret_inspect {{{
secret_inspect() {
    unset _FORMAT
    _RAW=0
    while [ -n "$1" ]; do
        case "$1" in
            -f|--format)
                if [ "$#" -lt "2" ]; then
                    printf '%s: error: argument expected after "%s"\n' \
                        "${0##*/}" "$1" >&2
                fi
                _FORMAT="$2"
                shift 2 ;;
            -h|--help)
                cat >&2 <<'EOF'
usage: secret inspect [-f FORMAT/--format FORMAT] [-h/--help] NAME

optional arguments:
  -f FORMAT/--format FORMAT
                   format inspect output using a JQ filter expression
  -h/--help        display this message and exit
  -r/--raw-output  request raw output from JQ

positional arguments:
  NAME  the name of the secret
EOF
                return 0 ;;
            -r|--raw-output) _RAW="1"; shift;;
            # {{{
            -?|--?*)
                printf -- '%s: error: unknown optional argument "%s"\n' \
                    "${0##*/}" "$1" >&2
                return 2 ;;
            -?*)
                _TAIL="${1#??}" _HEAD="${1%"$_TAIL"}"
                shift
                set -- "$_HEAD" "-$_TAIL" "$@" ;;
            *) break;;
            # }}}
        esac
    done
    if [ -z "$1" ]; then
        printf '%s: error: missing argument NAME\n' "${0##*/}" >&2
        return 2
    elif ! expr "$1" : '^[0-9A-Z_a-z-]\{1,\}$' >/dev/null; then
        printf '%s: error: invalid name format "%s"\n' "${0##*/}" "$1" >&2
        return 2
    elif ! secret_exists "$1"; then
        printf '%s: error: no secret with name "%s"\n' "${0##*/}" "$1" >&2
        return 2
    fi
    exec 6>&2
    exec 2>/dev/null
    if [ -z "$_FORMAT" ]; then
        gpg -do - -q "$SECRETS_DIR/$1.gpg"
    elif [ "$_RAW" -eq "0" ]; then
        gpg -do - -q "$SECRETS_DIR/$1.gpg" | jq -M "$_FORMAT" -
    else
        gpg -do - -q "$SECRETS_DIR/$1.gpg" | jq -Mr "$_FORMAT" -
    fi
    exec 2>&6 6>&-
}
# }}}

# secret_ls {{{
secret_ls() {
    while [ -n "$1" ]; do
        case "$1" in
            -h|--help)
                cat >&2 <<'EOF'
usage: secret ls [-h/--help]

optional arguments:
  -h/--help  display this message and exit
EOF
                return 0 ;;
            # {{{
            -?|--?*)
                printf -- '%s: error: unknown optional argument "%s"\n' \
                    "${0##*/}" "$1" >&2
                return 2 ;;
            -?*)
                _TAIL="${1#??}" _HEAD="${1%"$_TAIL"}"
                shift
                set -- "$_HEAD" "-$_TAIL" "$@" ;;
            *) break;;
            # }}}
        esac
    done
    find "$SECRETS_DIR" -depth 1 -name '*.gpg' | \
        sed -e 's/.*\///' -e 's/\.gpg$//' | sort
}
# }}}

# secret_rm {{{
secret_rm() {
    _ALL=0
    _IGNORE=0
    while [ -n "$1" ]; do
        case "$1" in
            -a|--all) _ALL=1; shift;;
            -h|--help)
                cat >&2 <<'EOF'
usage: secret rm [-h/--help] [-i/--ignore] NAME [NAME...]
       secret rm [-h/--help] [-a/--all]

optional arguments:
  -a/--all     remove all secrets
  -h/--help    display this message and exit
  -i/--ignore  ignore if the secret is missing

positional arguments:
  NAME  the name of a secret
EOF
                return 0 ;;
            -i|--ignore) _IGNORE=1; shift;;
            # {{{
            -?|--?*)
                printf -- '%s: error: unknown optional argument "%s"\n' \
                    "${0##*/}" "$1" >&2
                return 2 ;;
            -?*)
                _TAIL="${1#??}" _HEAD="${1%"$_TAIL"}"
                shift
                set -- "$_HEAD" "-$_TAIL" "$@" ;;
            *) break;;
            # }}}
        esac
    done
    if [ "$_ALL" -eq "1" ]; then
        find "$SECRETS_DIR" -depth 1 -name '*.gpg' -exec rm '{}' \;
    else
        if [ "$#" -eq 0 ]; then
            printf '%s: error: missing argument NAME\n' "${0##*/}" >&2
            return 2
        fi
        while [ "$#" -gt 0 ]; do
            if ! expr "$1" : '^[0-9A-Z_a-z-]\{1,\}$' >/dev/null; then
                printf '%s: error: invalid name format "%s"\n' "${0##*/}" \
                    "$1" >&2
                return 2
            fi
            _NAME="$1"
            if secret_exists "$1"; then
                rm "$SECRETS_DIR/$1.gpg"
            elif [ "$_IGNORE" -ne "1" ]; then
                printf '%s: error: no secret with name "%s"\n' "${0##*/}" \
                    "$1" >&2
                return 2
            fi
            shift
        done
    fi
    return 0
}
# }}}

if [ -z "$SECRETS_DIR" ]; then
    printf '%s: error: missing variable SECRETS_DIR\n' "${0##*/}" >&2
    exit 2
elif [ ! -d "$SECRETS_DIR" ]; then
    printf '%s: error: missing secrets directory "%s"\n' "${0##*/}" \
        "$SECRETS_DIR" >&2
    exit 2
elif [ -z "$SECRETS_GPGUSERID" ]; then
    printf '%s: error: missing variable SECRETS_GPGUSERID\n' "${0##*/}" >&2
    exit 2
fi

while [ -n "$1" ]; do
    case "$1" in
        create) COMMAND="secret_create"; shift; break;;
        exists) COMMAND="secret_exists"; shift; break;;
        inspect|show) COMMAND="secret_inspect"; shift; break;;
        ls|list) COMMAND="secret_ls"; shift; break;;
        rm) COMMAND="secret_rm"; shift; break;;
        -h|--help)
            cat >&2 <<EOF
usage: secret [-h/--help] COMMAND ARGUMENTS...

optional arguments:
  -h/--help  display this message and exit

positional arguments:
  COMMAND  one of create, exists, inspect, ls/list, or rm

commands:
  create        create a new secret
  exists        check if a secret exists
  inspect/show  inspect an existing secret
  ls/list       list existing secrets
  rm            remove a secret or secrets
EOF
            exit 0 ;;
        -?|--?*)
            printf -- '%s: error: unknown optional argument "%s"\n' \
                "${0##*/}" "$1" >&2
            exit 2 ;;
        -?*)
            _TAIL="${1#??}" _HEAD="${1%"$_TAIL"}"
            shift
            set -- "$_HEAD" "-$_TAIL" "$@" ;;
        *) break;;
    esac
done

if [ -z "$COMMAND" ]; then
    printf '%s: error: missing argument COMMAND\n' "${0##*/}" >&2
    exit 2
fi

"$COMMAND" "$@"
